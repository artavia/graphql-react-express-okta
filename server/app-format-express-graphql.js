if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

const { NODE_ENV , PORT , HOSTNAME } = process.env;

const IS_PRODUCTION_NODE_ENV = NODE_ENV === 'production';
const IS_DEVELOPMENT_NODE_ENV = NODE_ENV === 'development';

const express = require("express");
const app = express();

const cors = require("cors");
const compression = require("compression");
const path = require("path");

const graphqlRouter = require("./routes/graphql");

app.enable( 'trust proxy' , 1 );
app.disable( 'x-powered-by' );

app.set( 'etag' , false ); 
app.use( cors() );

app.use( compression() );

app.use( express.json() ); 
app.use( express.urlencoded( { extended: true } ) );

app.use( '/graphql' , graphqlRouter ); 

if( (IS_PRODUCTION_NODE_ENV === true) ){
  
  app.use( express.static( path.join( __dirname, 'public' ) ) ); 

  const catchAllGET = ( req, res, next ) => {
    res.sendFile( path.join( __dirname , 'public', 'index.html' ) );
  };
  app.get( '*' , catchAllGET ); 
}


app.listen( PORT, HOSTNAME, () => { 
  
  if( IS_PRODUCTION_NODE_ENV === true ){
    console.log( `\nProduction backend is running at http://${HOSTNAME}:${PORT} . \n`);
  }

  if( IS_DEVELOPMENT_NODE_ENV === true ){
    console.log( `\nDevelopment backend has started at http://${HOSTNAME}:${PORT} . \n`);
    console.log(`\nOpen the GraphQL playground at http://${HOSTNAME}:${PORT}/graphql \n`);
  }

} );
