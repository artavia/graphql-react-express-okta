if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

const { NODE_ENV , PORT , HOSTNAME } = process.env;

const IS_PRODUCTION_NODE_ENV = NODE_ENV === 'production';
const IS_DEVELOPMENT_NODE_ENV = NODE_ENV === 'development';

const express = require("express");
const app = express();

const cors = require("cors");
const compression = require("compression");
const path = require("path");
const { ApolloServer } = require("apollo-server-express"); 

const { typeDefs } = require("./graphql/apollo-server-express/typedefs");
const { resolvers } = require("./graphql/apollo-server-express/resolvers");
const { context } = require("./graphql/apollo-server-express/context");

app.enable( 'trust proxy' , 1 );
app.disable( 'x-powered-by' );
app.set( 'etag' , false ); 

app.use( cors() );
app.use( compression() );

app.use( express.json() ); 
app.use( express.urlencoded( { extended: true } ) ); 

if( (IS_PRODUCTION_NODE_ENV === true) ){
  
  app.use( express.static( path.join( __dirname, 'public' ) ) ); 

  const catchAllGET = ( req, res, next ) => {
    res.sendFile( path.join( __dirname , 'public', 'index.html' ) );
  };
  app.get( '*' , catchAllGET ); 
}

const server = new ApolloServer( {
  typeDefs: typeDefs
  , resolvers: resolvers
  , context: context
  , playground: IS_PRODUCTION_NODE_ENV ? false : true  
} );

server.applyMiddleware( { app: app } );

app.listen( PORT, HOSTNAME, () => { 
  
  if( IS_PRODUCTION_NODE_ENV === true ){
    console.log( `\nProduction backend is running at http://${HOSTNAME}:${PORT} . \n`);
  }

  if( IS_DEVELOPMENT_NODE_ENV === true ){
    console.log( `\nDevelopment backend has started at http://${HOSTNAME}:${PORT} . \n`);
    console.log(`\nOpen the GraphQL playground at http://${HOSTNAME}:${PORT}${server.graphqlPath} \n`);
  }

} );