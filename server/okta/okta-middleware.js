if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}
const { OKTA_ORG_URL, OKTA_CLIENT_ID, API_TOKEN_VALUE } = process.env;

const OktaJwtVerifier = require("@okta/jwt-verifier");
const oktaJwtVerifier = new OktaJwtVerifier( {
  clientId: `${OKTA_CLIENT_ID}`
  , issuer: `${OKTA_ORG_URL}/oauth2/default`
} );

const getUserIdFromToken = async ( token ) => {
  if(!token){
    return;
  }
  try{
    const jwt = await oktaJwtVerifier.verifyAccessToken( token, "api://default" ); 
    const { claims } = await jwt;
    const { sub } = await claims;
    return sub;
  }
  catch(error){
    console.log( "getUserIdFromToken error" , error );
  }
};

const { Client } = require("@okta/okta-sdk-nodejs");
const client = new Client( { orgUrl: OKTA_ORG_URL , token: API_TOKEN_VALUE } );

const getUser = async ( userId ) => {
  
  if(!userId){
    return;
  }
  try{ 
    const user = await client.getUser( userId );     
    const { profile } = await user; 
    return profile;
  }
  catch(error){
    console.log( "getUser error" , error );
  }
};

module.exports = { getUserIdFromToken, getUser };