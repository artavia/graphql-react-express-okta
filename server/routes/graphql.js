if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

const { NODE_ENV } = process.env;
const IS_PRODUCTION_NODE_ENV = NODE_ENV === 'production';

const express = require("express");
const graphqlRouter = express.Router();
const { graphqlHTTP } = require("express-graphql");

const { schema } = require("./../graphql/express-graphql/schema");
const { rootValue } = require("./../graphql/express-graphql/resolvers");
const { context } = require("./../graphql/express-graphql/context");

graphqlRouter.all( "*", graphqlHTTP( (req, res) => {
  
  return ({
    schema: schema
    , rootValue: rootValue
    , context: context( { req, res } )
    , graphiql: IS_PRODUCTION_NODE_ENV ? false : { headerEditorEnabled: true }  
  });

} ) );


module.exports = graphqlRouter;
