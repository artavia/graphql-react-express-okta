const { v4: uuidv4 } = require("uuid");
const { AuthenticationError } = require("apollo-server-express");

const { POSTS } = require("./../dummydata"); 
const { PEOPLE } = require("./../dummydata");

const resolvers = {
  
  Query : {
    
    posts: ( _, __ ) => { return POSTS; }

    , post: ( _, { id } ) => {

      const mypost = POSTS.find( el => {
        if( el.id === id ){
          return el;
        }
      } ); 

      return mypost;
    }

    , authors: ( _, __ ) => { return PEOPLE; }

    , author: ( _, { id } ) => {
  
      const myauthor = PEOPLE.find( el => {
        if( el.id === id ){ 
          return el;
        }
      } ); 

      return myauthor;
    }

  }
  
  , Mutation: {
    
    checkAuthor: ( _, { input: { id, name } } ) => {

      let person = { id, name }; 

      const doesPersonIdExist = PEOPLE.some( el => el.id === id ); 
    
      if( doesPersonIdExist === false ){

        let authorizedId = {};

        if( id !== undefined && id.length > 0 ){
          authorizedId = { "id": id };
        }

        if( id === undefined ){
          authorizedId = { "id": uuidv4() };
        }

        let massagedperson = { ...person, ...authorizedId }; 
        
        person = massagedperson; 

        PEOPLE.push( person ); 
      }

      if( doesPersonIdExist === true ){ 
  
        let myauthor = PEOPLE.find( el => {
          if( el.id === id ){ 
            return el;
          }
        } ); 
        
        person = myauthor; 
      }
  
      return person;
    }

    , submitPost: async ( _, { input: { id, author, content } }, context ) => {

      if( await !context.user ){ 

        throw new AuthenticationError("You must be logged in to perform this action");

      }

      const doesAuthorIdExist = await PEOPLE.some( el => el.id === author.id ); 
  
      if( await doesAuthorIdExist === false ){
  
        let massagedauthor = await { ...author, "id": uuidv4() }; 

        author = await massagedauthor; 

        await PEOPLE.push( await author ); 
      }

      if( await doesAuthorIdExist === true ){ 
        
        let myauthor = await PEOPLE.find( el => {
          if( el.id === author.id ){ 
            return el;
          }
        } ); 
  
        myauthor = await author; 
      }
  
      let post = await { id, author, content }; 
  
      const doesPostIdExist = await POSTS.some( el => el.id === id ); 
  
      if( await doesPostIdExist === false ){
  
        let massagedpost = await { ...post, "id": uuidv4() }; 
        
        post = await massagedpost; 

        await POSTS.push( await post ); 
      }
  
      if( await doesPostIdExist === true ){ 
        
        let mappedPosts = await POSTS.map( (el, idx, arr ) => { 
          if( el.id === id ){
            return { 
              splicedElement: el
              , spliceIndex: idx
            };
          }      
        } ); 
  
        const updateHelperObject = await mappedPosts.filter( (el) => {
          if(el !== undefined ){
            return el;
          }
        } )[0]; 
  
        let { spliceIndex } = await updateHelperObject; 
        
        await POSTS.splice( await spliceIndex, 1, await post );  
      }
  
      return await post;  
    }
    
  }

};

module.exports = { resolvers };
