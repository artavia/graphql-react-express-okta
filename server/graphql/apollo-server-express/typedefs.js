const { gql } = require("apollo-server-express");

const typeDefs = gql`
  
  input PostInput {
    id: ID
    author: AuthorInput!
    content: String!
  }

  input AuthorInput {
    id: ID
    name: String!
  }

  type Post {
    id: ID
    author: Author
    content: String
  }

  type Author {
    id: ID
    name: String
  }

  type Query {
    posts: [Post]
    post(id: ID!): Post
    authors: [Author]
    author(id: ID!): Author
  }
  
  type Mutation {
    submitPost( input: PostInput! ): Post
    checkAuthor( input: AuthorInput! ): Author
  }
`;

module.exports = { typeDefs };
