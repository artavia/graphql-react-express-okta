const { getUserIdFromToken, getUser } = require("./../../okta/okta-middleware");

const context = async ( props ) => {

  const { res } = await props;
  
  await res.setHeader('Content-Type', 'application/graphql; charset=utf-8'); 
  
  const { req } = await props;

  let authHeader;
  let accessToken;
  let sub;
  let user;

  if( await req.headers.authorization !== undefined ){
    
    authHeader = await req.headers.authorization;
    
    if( await authHeader !== undefined ){
      accessToken = await authHeader.split(" ")[1];
    }
  }

  if( await accessToken !== undefined ){
    sub = getUserIdFromToken( await accessToken );
  }

  if( await sub !== undefined ){
    user = getUser( await sub );
  }

  return {
    user: await user
  };
  
};

module.exports = { context };
