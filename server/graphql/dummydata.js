const { v4: uuidv4 } = require("uuid");

const POSTS = [];
const PEOPLE = [];

let getMyAuthorType = (author) => { 
  const myauthor = PEOPLE.find( el => {
    if( el.name === author ){
      return el;
    }
  } );
  return myauthor;
};

const initializeFakePeople = () => { 

  const DUMMYDATA = {
    
    fakePeople : [ { id: uuidv4(), name: "Georgie Kerlin" }  , { id: uuidv4(), name: "Billi Canilli" } ]

  };
  
  const { fakePeople } = DUMMYDATA;

  fakePeople.forEach( ( person ) => {
    return PEOPLE.push(person);
  } );

};

const initializeFakePosts = () => { 

  const DUMMYDATA = {

    fakePosts : [ 
      { id: uuidv4(), author: getMyAuthorType("Georgie Kerlin"), content: "This guy is farrgin' stupid! Molestias itaque ullam dolor nisi!" } , 
      { id: uuidv4(), author: getMyAuthorType("Billi Canilli"), content: "Aw, forfa!!! Ratione molestias ducimus." } 
    ] 

  };
  
  const { fakePosts } = DUMMYDATA;

  fakePosts.forEach( ( post ) => {
    return POSTS.push(post);
  } );

};

const initAll = () => {
  initializeFakePeople();
  initializeFakePosts(); 
};

initAll();

module.exports = { POSTS , PEOPLE };