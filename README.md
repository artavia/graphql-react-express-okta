# GraphQL, React, Express &amp; Okta

## Description
This is another re&#45;hash of a project by Braden Kelley on the subjects of GraphQL, React, and Express found at the Okta blog.

### Initial comments
All of this energy and focus is my way of addressing the shortfalls I encounter with processing and deciphering **bugs** with **ApolloGraphQL server**. Upon embarking on this project, it became clear that **@apollo/client** is also a thing. Ay, carambolas!

To illustrate the point, some older technologies on the client side had to be replaced with newer technologies like so&hellip;

Observe as I go **out with the old and/or leaky** packages&hellip;
  - apollo-boost 
  - graphql-tag 
  - react-apollo
  - final-form
  - react-final-form
  - reactstrap
  - npm-run-all
  
And, I come **in with the new and stable** packages&hellip;
  - @apollo/client

Therefore, in order to not feel as if I am experiencing a **car crash** when taking on a **crash course** I felt it was important to flesh out **the corresponding @apollo/client lessons** to fruition on the subjects of **server/client, as well as, queries and mutations on the front end**. This subject dealing with @apollo/client is important enough for me to put all else on hold as I take a day or two (or more&hellip;) to hammer out these new lessons that suddenly have taken a higher priority. The alternative would have been that I would have perenially remained behind and would have been chasing my tail in a torturous manner. It&rsquo;s a **HUGE pain in the tucas** to deconstruct archaic projects while simultaneously learning modernized newer major versions.

The eventual interim goal is to have a solid grip on the subjects of a) **@apollo/client** before addressing b) **Foreign Keys in the context of Sequelize during the seed phase(s)** prior to ultimately tackling c) **apollo-server**. 

Lord willing, everything should work out as planned. Consequently, you can expect to see everything posted to GitLab sometime in the next&hellip; well, your guess is as good as mine! 

## Attribution(s)
The original blog article called [Build a Simple Web App with Express, React and GraphQL](https://developer.okta.com/blog/2018/10/11/build-simple-web-app-with-express-react-graphql "link to okta blog") can be found at the okta blog. 

## What is different with this version?
I decided to use the **modern and/or @latest versions** inevitably bringing about the **breaking changes** that are guaranteed to materialize. And did it ever bring about profound breaking changes. Often what would happen is that I would start to visit a certain phase during reconstruction of this project in the evening, then, I would be unable to pick up where I had previously left off until the following evening or during the following morning more than 24 hours after I had initially began work.

In terms of the front facing Create React App project, there was a lot of investigative discovery involved. Upon visiting the corresponding some of the corresponding npmjs pages I found that some of the packages are deprecated while, on the other hand, they have been replaced by other packages that contain entirely different procedures for object instantiation and operation. The changes are profound. 

## This is one of many baby steps to cure personal shortfalls in deciphering Sequelize errors
I have in recent weeks attempted to complete the [ApolloGraphQL fullstack tutorial](https://www.apollographql.com/docs/tutorial/introduction/ "link to tutorial") (or, **AFT** for short) on two different occasions. The authors **purported** that one does not need to know the other minor themes in order to complete the lesson. That would be nice if it were true, however, it was not meant to be so.

This exercise is a refresher on the subject of **GraphQL** and is a component of the warm&#45;up phase so I can finally ease back into the completion of the aforementioned exercise.

I thank you for your interest.

### God bless