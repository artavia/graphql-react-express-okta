import React from "react";

import { BrowserRouter, Route, Switch } from "react-router-dom";

import { useHistory } from 'react-router-dom';

import { Security, LoginCallback, SecureRoute } from "@okta/okta-react";

import { securityConfigurationObject } from "../custom-okta-security-configuration/config";

import { REACT_APP_REDIRECT_PATH } from "../custom-okta-security-configuration/development-environment-variables";

import { Home } from "./../components/pages/home";
import { Admin } from "./../components/pages/admin";
import { FauxSuperAdmin } from "./../components/pages/faux-super-admin";

import { Unauthenticated } from "./../components/pages/unauthenticated";
import { AlphaTest } from "./../components/pages/alphatest";

import { DNEPage } from "./../components/pages/DNEPage";
import { Main } from "./main";

const HasAccessToBrowserRouter = () => {

  const history = useHistory();

  const customizedSignInWidget = () => {
    history.push("/unauthenticated");
  };
  
  const element = (
    <>
      <Security {...securityConfigurationObject} onAuthRequired={ customizedSignInWidget }>
        <Main>
          <Switch>

            <Route exact={ true } path="/" component={ Home } />

            {/* <Route exact={ true } path="/admin" component={ Admin } /> */}
            <SecureRoute exact={true} path="/admin" component={ Admin }/>

            <SecureRoute exact={true} path="/faux-super-admin" component={ FauxSuperAdmin }/>
            
            <Route exact={ true } path="/unauthenticated" component={ Unauthenticated } />
            
            <SecureRoute exact={true} path="/alphatest" component={ AlphaTest }/>

            <Route exact={true} path={ REACT_APP_REDIRECT_PATH } component={ LoginCallback } />

            <Route component={ DNEPage } />

          </Switch>
        </Main>
      </Security>
    </>
  );

  return element;
}; 


const Alpha = () => {
  
  const el = (
    <BrowserRouter>
      <HasAccessToBrowserRouter />
    </BrowserRouter>
  );

  return el;
}; 

export { Alpha };
