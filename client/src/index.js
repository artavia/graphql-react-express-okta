import React from 'react';
import ReactDOM from 'react-dom';

import { ApolloProvider } from "@apollo/client";  
// import { ApolloProvider } from "react-apollo"; // TO BE CULLED...

import { client } from "./apolloclient/apolloclient";

import "./sass/boots.scss";
import "./sass/customstyles.scss";

import "./js/vendors.js";

import {App} from './App';
import * as serviceWorker from './serviceWorker';


ReactDOM.render( 
  <React.StrictMode>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </React.StrictMode> 
  , document.querySelector("#root")
); 

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

// if (module.hot) { module.hot.accept(); }
