import React , { useState, useEffect, useRef, useCallback } from "react";

import { SharedModal } from "./../shared/SharedModal";
import { FauxSuperAddForm } from "./../forms/faux-super-addform";
import { EditForm } from "./../forms/editform";

const FauxSuperPostEditor = ( props ) => {
  
  let { post, onClose } = props;  
  
  let isMountedRef = useRef(null); 
  const [ showModal, setShowModal ] = useState( false ); 

  const leakSilencer = useCallback( () => {
    
    isMountedRef.current = true;

    if(isMountedRef.current){
      setShowModal(true);
    }
    return () => {
      wipeItOut();
    }
    
  } , [] );

  useEffect( () => {
    leakSilencer();
  } , [ leakSilencer ] );

  const parentStateHandler = () => { onClose(); };
  const initializeModalCloseHandler = () => { parentStateHandler(); };
  const finalizePostHandler = () => { initializeModalCloseHandler(); }; 

  const wipeItOut = () => { 
    setShowModal(false); 
    isMountedRef.current = false;
  };

  let el = (
    <SharedModal finalizePostHandler={ finalizePostHandler } post={post}>
      
      {
        post.id 

        ? (<EditForm finalizePostHandler={ finalizePostHandler } post={post} />) 

        : (<FauxSuperAddForm />)
      }

    </SharedModal>
  ); 

  if( showModal ){ return el; }
  if( !showModal ){ return null; }

};

export { FauxSuperPostEditor };