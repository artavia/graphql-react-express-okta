import React from "react";
import { useQuery /* , NetworkStatus */ } from "@apollo/client";

import { SharedTable } from "./../shared/SharedTable";
import { SharedTableRow } from "./../shared/SharedTableRow";
import { DisclaimerLoading } from "./../elements/disclaimerloading";
import { DisclaimerError } from "./../elements/disclaimererror";

import { GET_POSTS } from "./../../models-graphql-endpoints/query-posts";

// const refetchResults = (refetch) => { refetch(); }; 

const rowStyles = ( post, canEdit ) => canEdit(post) ? { cursor: "pointer"  } : {};

const rowclickhandler = (event,canEdit,onEdit, post) => { 
  event.preventDefault(); 
  return canEdit(post) && onEdit(post);
};

const dataList = ( obj ) => { 

  let { data } = obj; 
  
  let { canEdit, onEdit } = obj.props; 

  let { userInfo } = obj.props; 
  
  let additionalFields = {
    rowStyles: rowStyles
    , canEdit: canEdit
    , onEdit: onEdit
    , rowclickhandler: rowclickhandler
    , userInfo: userInfo
  }; 
  
  // return data.posts.map( ( post ) => {
  return data.posts.map( ( post, idx ) => { 

    // Warning: Encountered two children with the same key
    return (<SharedTableRow key={ `${idx}-${post.id}` } post={ post } { ...additionalFields } />);

  } );

};

const PostViewer = ( props ) => {  

  // console.log( "PostViewer props", props ); 
  
  // const { loading, error, data, refetch, networkStatus } = useQuery( GET_POSTS ); 
  const { loading, error, data } = useQuery( GET_POSTS ); 

  // if( networkStatus === NetworkStatus.refetch ) { return <DisclaimerRefetching />; }
  if( loading ){ return <DisclaimerLoading />; }
  if( error ){ return <DisclaimerError error={error} />; }
  
  
  // console.log( "data" , data );
  // console.log( "data.posts" , data.posts );
  
  if(!loading){
    return (
      <>
        {/* <div className="container">
          <div className="row">
            <button type="button" className="btn btn-warning mb-2 mr-1" onClick={ () => refetchResults(refetch) }>
              Refetch that thang!!!
            </button>
          </div>
        </div> */}

        <SharedTable>
          { dataList( { data , props } ) }
        </SharedTable>
      </>
    );
  }
};

PostViewer.defaultProps = {
  canEdit: () => false
  , onEdit: () => null
};

export { PostViewer };