import React from "react";

const SharedTable = ( props ) => {
  
  let tablebody = (
    <>
      <div className="container">
        <div className="row">
          <div className="table-responsive-sm">
            <table className="table">
              <thead className="thead-dark">
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Author Name</th>
                  <th scope="col">Author ID</th>
                  <th scope="col">Content</th>
                </tr>
              </thead>
              <tbody>
                { props.children }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );

  return tablebody;
  
};

export { SharedTable };
