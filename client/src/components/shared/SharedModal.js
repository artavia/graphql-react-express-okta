import React from "react";
import { useEffect } from "react"; 
import { useCallback } from "react"; 
import { useRef } from "react"; 
import $ from 'jquery';

const SharedModal = ( props ) => { 
  
  let { finalizePostHandler } = props; 
  let { post } = props;
  let modalRef = useRef();

  const blingAndBoots = useCallback( () => { 
    // console.log( "$", $ );
    $(modalRef).modal('show');
    $(modalRef).on( 'hidden.bs.modal', finalizePostHandler );
  } , [ finalizePostHandler ] );

  useEffect( () => {
    blingAndBoots();
  } , [ blingAndBoots ] );

  // const toggleModalClosed = () => {
  //   $(modalRef).modal( 'hide' );
  //   finalizePostHandler();
  // };

  let el = (
    <div 
      className="modal fade" 
      ref={ modal => modalRef = modal }
      id="exampleModal" 
      tabIndex="-1" 
      role="dialog" 
      aria-labelledby="exampleModalLabel" 
      aria-hidden="true"
    >
      
      <div className="modal-dialog" role="document">
        <div className="modal-content">

          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">{!post.id ?"Add New Post":"Edit Post"}</h5>
            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div className="modal-body">
            { props.children }
          </div>

          {/* <div className="modal-footer">
            <button type="button" className="btn btn-secondary" onClick={toggleModalClosed}>Cancel</button>
          </div> */}

        </div>
      </div>

    </div>
  );

  return el;
};

export { SharedModal };