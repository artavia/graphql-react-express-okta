import React from "react";

const SharedTableRow = ( props ) => { 

  const { canEdit, onEdit, rowStyles, rowclickhandler } = props; 
  
  const { post } = props;
  const { id, author, content } = post; 
  const { name, id:authorId } = author; 
  
  let additionalProps; 

  if( props.userInfo !== undefined ){
    
    const { userInfo } = props;
    
    if( userInfo !== null ){
      
      const { sub } = userInfo; 

      if( sub === authorId ) { 
        additionalProps = {
          post: post,
          style: rowStyles( post, canEdit )
          , onClick: event => rowclickhandler( event, canEdit, onEdit, post )
        };
      } 

      if( sub !== authorId ) { 
        additionalProps = {}; 
      }

    }

  } 
  
  if( !canEdit() ){ 
    additionalProps = {}; 
  }
  
  const el = (
    <>
      <tr key={ post.id } {...additionalProps} >
        <th id={id} scope="row">{id}</th>
        <td id={name}>{name}</td>
        <td id={authorId}>{authorId}</td>
        <td id={`content-${id}`}>{content}</td>
      </tr>
    </>
  );
  
  return el;

};

export { SharedTableRow };