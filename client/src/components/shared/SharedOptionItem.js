import React from "react";

const SharedOptionItem = ( props ) => {

  let { author } = props;
  let { id:authorId, name } = author;
  
  let dataobj = { id: authorId, name: name };
  let strungout = JSON.stringify( dataobj );

  let el = (
    <>
      <option id={authorId} value={ strungout } >
        { name }
      </option>
    </>
  );

  return el;
  
};

export { SharedOptionItem };
