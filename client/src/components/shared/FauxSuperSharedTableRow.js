import React from "react";

const FauxSuperSharedTableRow = ( props ) => { 

  const { canEdit, onEdit, rowStyles, rowclickhandler } = props;
  
  const { post } = props;
  const { id, author, content } = post; 
  const { name, id:authorId } = author; 
  
  let additionalProps;
  
  
  if( !canEdit() ){ 
    additionalProps = {}; 
  }
  
  if( canEdit() ){ 
    additionalProps = {
      post: post,
      style: rowStyles( post, canEdit )
      , onClick: event => rowclickhandler( event, canEdit, onEdit, post )
    };
  }

  let el = (
    <>
      <tr key={ post.id } {...additionalProps} >
        <th id={id} scope="row">{id}</th>
        <td id={name}>{name}</td>
        <td id={authorId}>{authorId}</td>
        <td id={`content-${id}`}>{content}</td>
      </tr>
    </>
  );
  
  return el;

};

export { FauxSuperSharedTableRow };