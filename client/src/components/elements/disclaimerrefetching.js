import React from "react";

const DisclaimerRefetching = () => {

  let el = (
    <>
      <div className="container">
        <div className="row">
          <p>Refetching&hellip;</p>
        </div>
      </div>
    </>
  );

  return el;
};

export { DisclaimerRefetching };