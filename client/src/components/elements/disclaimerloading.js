import React from "react";

const DisclaimerLoading = () => {

  let el = (
    <>
      <div className="container">
        <div className="row">
          <p>Loading&hellip;</p>
        </div>
      </div>
    </>
  );

  return el;
};

export { DisclaimerLoading };