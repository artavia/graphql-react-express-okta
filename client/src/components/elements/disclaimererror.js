import React from "react";

const DisclaimerError = ( {error} ) => { 

  let el;

  let { message: errorMessage } = error;

  if( errorMessage !== null ){

    console.log( "errorMessage", errorMessage );
    
    el = (
      <>
        <div className="container">
          <div className="row">
            Error: 
            <code>
              { errorMessage }
            </code>
          </div>
        </div>
      </>
    );

  } 
  
  let { networkError } = error; 
  if( networkError !== null ){

    let { message: networkerrorMessage } = networkError;

    if ( networkerrorMessage !== null ) {
    
      console.log( "networkerrorMessage", networkerrorMessage );
      
      el = (
        <>
          <div className="container">
            <div className="row">
              networkError: 
              <code>
                { networkerrorMessage }
              </code>
            </div>
          </div>
        </>
      );
      
    }

    let { result } = networkError;

    if( result !== undefined ){
      
      let { errors } = result;
      let graphQLError = errors[0];
      let { message: graphqlerrorMessage } = graphQLError;
      
      el = (
        <>
          <div className="container">
            <div className="row">
              graphQL Error: 
              <code>
                { graphqlerrorMessage }
              </code>
            </div>
          </div>
        </>
      );

    }

  }
  
  return el;
  
};

export { DisclaimerError };