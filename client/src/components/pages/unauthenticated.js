import React, { useState, useEffect, useCallback } from "react";

import * as OktaSignIn from "@okta/okta-signin-widget";
import "@okta/okta-signin-widget/dist/css/okta-sign-in.min.css";

import { securityConfigurationObject } from "./../../custom-okta-security-configuration/config";
import { REACT_APP_OKTA_ORG_URL } from "./../../custom-okta-security-configuration/development-environment-variables";

import SVENGALLIO from "./../../svg/logo.svg";

const { clientId, issuer, redirectUri, pkce, scopes } = securityConfigurationObject;

const Unauthenticated = () => {

  const [ user, setUser ] = useState(false);

  const innerFunction = useCallback( () => {
    const goGetEm = async () => {
      
      const signInConfig = {
        baseUrl: REACT_APP_OKTA_ORG_URL
        , el: "#signIn"
        , logo: `${SVENGALLIO}`
        , authParams: {
          pkce: pkce
          , issuer: issuer
        }
      };
      
      const signIn = new OktaSignIn( signInConfig );
  
      return await Promise.resolve( signIn );
  
    };

    goGetEm().then( async (signIn) => {

      signIn.remove();

      const getTokensConfig = {
        clientId: clientId
        , redirectUri: redirectUri
        , getAccessToken: true  // Return an access token from the authorization server 
        , getIdToken: true  // Return an ID token from the authorization server
        , scope: "openid profile"
      };

      signIn.showSignInToGetTokens( getTokensConfig );

      return signIn;

      
    } )
    .then( ( signIn ) => {

      const authClient = signIn.authClient;
      const session = authClient.session.get();
      return { authClient, session, signIn };

    } ).then( async ({authClient, session, signIn }) => {
      console.log( ">>>>> await session.status: \n\n" , await session.status );
  
      // Session exists, show logged in state
      if( await session.status === "ACTIVE" ){
        
        // set username in state
        setUser( await session.login );
        await localStorage.setItem( "isAuthenticated" , "true" );
        
        console.log( ">>>>> await user \n\n" , await user );
  
        // get access and ID tokens
        await authClient.token.getWithoutPrompt( {
          scopes: scopes
        } )
        .then( async ( tokens ) => {
          
          console.log( ">>>>> await tokens \n\n" , await tokens );
          
          await tokens.forEach( async (token) => {
            
            if( await token.idToken ){
              await authClient.tokenManager.add( "idToken", token);
            }
            if( await token.accessToken ){
              await authClient.tokenManager.add( "accessToken", token);
            }
  
          } );
  
          await authClient.tokenManager.get("idToken").then( ( idToken ) => {
            
            console.log( `>>>>> Hello, \n\n ${idToken.claims.name} (${idToken.claims.email})` );
  
            // window.location.reload(); // THIS IS A HACK... REDO 
          } );
  
        } )
        .catch( ( error ) => {
          console.error( "thennable catch statement error" , error );
        } );
  
        return;
      }
      
    } );

  } , [ user ] ); // Update if user changes
  

  useEffect( () => {
    
    innerFunction();
    return () => {};

  } , [ innerFunction ] );
  
  let el = (
    <>
      <div className="container-fluid"> 

        <div className="py-5">
          <div className="row">
            <div className="col-md-6 mx-auto">
              <div className="unauthenticated">
                <h2 className="text-center">Please login, babe!</h2>
                
                <p>You need to sign in to continue. After you <strong>login</strong> you can, then, visit the restricted areas.</p>

                <div>
                  <div id="signIn"></div>
                </div>

              </div>
            </div>
          </div>
        </div>

      </div>
    </>
  );
  return el;
};

export { Unauthenticated };