import React, { useState } from "react";
import { useEffect, useCallback } from "react";
import { useRef } from "react";

import { useOktaAuth } from "@okta/okta-react";

import { gql, useMutation } from "@apollo/client";
import { CHECK_AUTHOR } from "./../../models-graphql-endpoints/mutation-checkauthor";

import { PostViewer } from "./../prezzos/postviewer";
import { PostEditor } from "./../prezzos/posteditor";


const Admin = () => {

  const [
    addAuthor
  ] = useMutation( 
    CHECK_AUTHOR
    , { 

      onCompleted: () => { console.log( "Mutation has completed" ); }
      , onError: (error) => { console.log( "An error has occurred: " , error ); } 

      , update( cache , { data: { submitAuthor } } ){ 

        cache.modify( {
          
          fields: {
            authors( existingAuthors = [] ) {
              
              const newAuthorRef = cache.writeFragment( {
              
                data: submitAuthor
                
                , fragment: gql`
                  fragment NewAuthor on Author {
                    id
                    name
                  }
                `
              } );

              return [ ...existingAuthors, newAuthorRef ];

            }
          }

        } ); 

      } // end update(){}

    } 
  ); 
  
  const [ editing, setEditing ] = useState(null);

  const [ userInfo, setUserInfo ] = useState(null);
  
  const { authState , authService } = useOktaAuth();

  let authorizedUserRef = useRef();
  
  const checkUser = useCallback( () => {
    
    if( !authState.isAuthenticated ){
      setUserInfo( null );
    }
    
    else {
      authService.getUser()
      .then( async (info) => {
        
        await setUserInfo( info );
        
      } );
    }
  } , [ authState, authService ] ); 

  const setStorage = useCallback( async () => {
    
    if( await authState.isAuthenticated ){ 
      const accessToken = await authState.accessToken;
      await localStorage.setItem( "token" , await accessToken ); 
    }

  } , [ authState.accessToken, authState.isAuthenticated ] ); 

  useEffect( () => { 
    checkUser();
    setStorage();

  } , [ checkUser, setStorage ] ); 

  const hammerTime = async () => {
    
    if( await authState.isAuthenticated && await userInfo !== null && await authorizedUserRef.current === undefined ){
      
      authorizedUserRef.current = { id: await userInfo.sub , name: await userInfo.name };
      
      let authorinput = { ...authorizedUserRef.current };
      
      await addAuthor( {
        
        variables: { authorinput: await authorinput }

        , optimisticResponse: {
          __typename: "Mutation"
          , submitAuthor: {
            __typename: "Author"
            , id: await authorinput.id
            , name: await authorinput.name
          }
        }
        
      } );
    }
    
  };

  hammerTime();

  const resetEditorHandler = () => {
    setEditing( null ); 
  };
  
  const createNewPostHandler = () => {
    setEditing( {} ); 
  };
  
  const editExistingPostHandler = (post) => {
    setEditing( post ); 
  };

  let el = (
    <>
      <div className="container-fluid">
        <div className="row">
          
          <div className="mt-4 mx-auto card">
            
            { authState.isPending && (<><div className="card-body"><h2>Your request is pending and loading&hellip;</h2></div></>) }

            { userInfo !== null && (<><div className="card-body"><h2>Welcome back, { userInfo.name }!</h2></div></>) }
            
          </div>
          
        </div>
      </div>

      <div className="container-fluid"> 

        <h3>Edit Posts</h3>

        { editing ? (<PostEditor post={ editing } onClose={ resetEditorHandler } />) : null }
        
        <div className="row">
          <button id="objectbutton" type="button" className="my-2 btn btn-primary btn-lg" onClick={ createNewPostHandler }>
            New Post
          </button>
        </div>

        <div className="row">
          <PostViewer userInfo={userInfo} canEdit={ () => true } onEdit={ editExistingPostHandler } />
        </div>

      </div>
    </>
  );
  return el;
};

export { Admin };