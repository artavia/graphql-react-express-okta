import React from "react";

import { useState, useEffect, useCallback } from "react";

import { NavLink /* , useHistory */ } from "react-router-dom";
import { useOktaAuth } from "@okta/okta-react";

import { PostViewer } from "./../prezzos/postviewer";

const Home = () => {

  const [ userInfo, setUserInfo ] = useState(null);

  const { authState, authService } = useOktaAuth();

  const checkUser = useCallback( () => {
    
    if( !authState.isAuthenticated ){
      setUserInfo( null );
    }
    
    else {
      authService.getUser()
      .then( async (info) => {        
        await setUserInfo( info );
      } );
    }
  } , [ authState, authService ] ); 

  useEffect( () => { 
    checkUser();
    // return () => {};
  } , [ checkUser ] ); 

  const login = (event) => {
    event.preventDefault();
    authService.login("/admin");
  };

  const logout = (event) => {
    event.preventDefault();
    
    authService.logout("/"); 

    // CULL THE COOKIES
    // care of... https://stackoverflow.com/questions/179355/clearing-all-cookies-with-javascript
    document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });

    // CULL ANY LOCALSTORAGE
    window.localStorage.clear();
  };

  const elPending = (
    <>
      <div className="card-body">
        <h2>Your request is pending and loading&hellip;</h2>
      </div>
    </>
  );

  const elNotAuthenticated = (
    <>
      <div className="card-body">

        <h2>You are not signed in yet.</h2>
        <p>By all means please login!</p>
        
        <NavLink role="button" to={""} className="btn btn-outline-success navbar-btn my-2 my-sm-0 mr-sm-2" onClick={login}>
          Login
        </NavLink>

      </div>
    </>
  );

  const elAuthenticated = (
    <>
      <div className="card-body">
        
        <h2>Welcome back{ userInfo !== null && (<>, { userInfo.name }</>) }!</h2>
        <p>You are authenticated!!!</p>

        <NavLink role="button" to={""} className="btn btn-outline-danger navbar-btn my-2 my-sm-0 mr-sm-2" onClick={logout}>
          Logout
        </NavLink>

      </div>
    </>
  );

  let el = (
    <>
      <div className="container-fluid">
        <div className="row">
          
          <div className="mt-4 mx-auto card">
            
            { !authState.isAuthenticated && elNotAuthenticated }
            { authState.isAuthenticated && elAuthenticated }
            { authState.isPending && elPending }
            
          </div>
          
        </div>
      </div>

      <div className="container-fluid">
        
        <h3>View Posts</h3>
        
        <div className="row">
          <PostViewer />
        </div>

      </div>
    </>
  );
  return el;
};

export { Home };