import React, { useState, useRef, useEffect, useCallback } from "react";
import $ from 'jquery';
import { gql, useQuery, useMutation } from "@apollo/client";

import { SharedOptionItem } from "./../shared/SharedOptionItem";

import { DisclaimerLoading } from "./../elements/disclaimerloading";
import { DisclaimerMutationCalled } from "./../elements/disclaimermutationcalled";
import { DisclaimerError } from "./../elements/disclaimererror";

import { GET_AUTHORS } from "./../../models-graphql-endpoints/query-authors";
import { SUBMIT_POST } from "./../../models-graphql-endpoints/mutation-submitpost";

const FauxSuperAddForm = ( props ) => { 

  let [ selectedAuthorString, setSelectedAuthorString ] = useState("");
  let [ author, setAuthor ] = useState("");
  let [ content, setContent ] = useState("");
  let [ name, setName ] = useState("");
  
  let selectItemRef = useRef();
  let newModalRef = useRef(null);
  let textAreaRef = useRef();
  let newAuthorRef = useRef(); 

  let extraModalReference = useCallback( () => {
    if( document.querySelector("#exampleModal") !== null ){
      newModalRef.current = document.querySelector("#exampleModal"); 
    }
    return () => {
      newModalRef.current = null;
    };
  } , [] );

  useEffect( () => {
    extraModalReference();
  } , [ extraModalReference ] );

  let errorCB = () => { console.log("There are errors!"); };

  let completedCB = () => { 
    // console.log("Mutation has completed"); 
    toggleModalClosed();
  };

  const toggleModalClosed = () => {
    $(newModalRef.current).modal( 'hide' );
  };

  const [
    modifyPosts
    , { loading, error, called } 
  ] = useMutation( 
    SUBMIT_POST
    , { 
      onCompleted: completedCB
      , onError: errorCB

      , update( cache , { data: { submitPost } } ){ 

        cache.modify( {
          
          fields: {
            posts( existingPosts = [] ) {
              
              const newPostRef = cache.writeFragment( {
              
                data: submitPost
                
                , fragment: gql`
                  fragment NewPost on Post {
                    id
                    author
                    content
                  }
                `
              } );

              return [ ...existingPosts, newPostRef ];

            }
          }

        } ); 

      } // end update(){}

    } 
  ); 

  const onChangeAuthor = ( event ) => {
    setName( newAuthorRef.current.value ); 
    if( newAuthorRef.current.value !== "" ){
      setAuthor( { name: newAuthorRef.current.value } );
      setSelectedAuthorString("");
    }
  };

  const onChangePostContent = ( event ) => { 
    setContent( textAreaRef.current.value ); 
  };

  const handleChange = async ( event ) => {
    setSelectedAuthorString( selectItemRef.current.value ); 
    if( selectItemRef.current.value !== "" ){
      setAuthor( JSON.parse( selectItemRef.current.value ) );
      setName("");
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    let postinput = { author: {...author} , content: content }; 

    modifyPosts( {
      variables: { postinput: postinput }
      , optimisticResponse: {
        __typename: "Mutation"
        , submitPost: {
          __typename: "Post"
          , id: undefined
          , author: postinput.author
          , content: postinput.content
        }
      }
    } );

  }; 

  

  const { loading: authorsLoading, error: authorsError, data: authorsData } = useQuery(GET_AUTHORS);
  
  if(authorsLoading){ return <DisclaimerLoading />; }
  if(authorsError){ return <DisclaimerError error={authorsError} />; }
  
  if(!authorsLoading){

    let options = authorsData.authors.map( ( author ) => {
      return (<SharedOptionItem key={ author.id } author={ author } />);
    } );

    return (
      <>
        <div className="container">

          <form className="form" onSubmit={ handleSubmit } >
            
            <div className="form-group">
              <label htmlFor="inputExistingAuthor">Author</label>
              <select id="inputExistingAuthor" onChange={ handleChange } className="form-control rounded-0" value={ selectedAuthorString } ref={ selectItemRef }>
                <option defaultValue="" value="" >Choose an Author</option>
                { options }
              </select>
            </div>

            <div className="form-group">
              <label htmlFor="name">New Author</label>
              <input type="text" ref={newAuthorRef} name="name" id="name" className="form-control rounded-0" placeholder="Joe Blow" value={ name } onChange={ onChangeAuthor } />
            </div>

            <div className="form-group">
              <label htmlFor="content">Post Content</label>
              <textarea ref={textAreaRef} value={ content } onChange={ onChangePostContent } className="form-control rounded-0" rows="2" name="content" id="content" placeholder="Bla, bla, bla!!!"></textarea>
            </div>

            <div className="form-group">
              <button type="submit" disabled={ content === '' || ( selectedAuthorString === '' && name === '' ) } className="btn btn-primary float-right mr-1">Submit</button>
              <button type="button" className="btn btn-secondary float-right mr-1" onClick={toggleModalClosed}>Cancel</button>
            </div>

          </form>

          { called && (<DisclaimerMutationCalled />) }
          
          { loading && (<DisclaimerLoading />) }

          { error && (<DisclaimerError error={error} />) }

        </div>
      </>
    );
  }
  
};

export { FauxSuperAddForm };
