import React, { useState, useRef, useEffect, useCallback } from "react";
import $ from 'jquery';
import { gql, useQuery, useMutation } from "@apollo/client";

import { useOktaAuth } from "@okta/okta-react";

import { SharedOptionItem } from "./../shared/SharedOptionItem";

import { DisclaimerLoading } from "./../elements/disclaimerloading";
import { DisclaimerMutationCalled } from "./../elements/disclaimermutationcalled";
import { DisclaimerError } from "./../elements/disclaimererror";


import { GET_ONE_AUTHOR } from "./../../models-graphql-endpoints/query-author";

import { SUBMIT_POST } from "./../../models-graphql-endpoints/mutation-submitpost";

const AddForm = ( props ) => { 

  let [ selectedAuthorString, setSelectedAuthorString ] = useState("");
  let [ author, setAuthor ] = useState("");
  let [ content, setContent ] = useState("");

  const [ userInfo, setUserInfo ] = useState(null);
  const { authState , authService } = useOktaAuth();
  

  let selectItemRef = useRef();
  let newModalRef = useRef(null);
  let textAreaRef = useRef();

  const checkUser = useCallback( () => {
    
    if( !authState.isAuthenticated ){
      setUserInfo( null );
    }
    
    else {
      authService.getUser()
      .then( async (info) => {

        await setUserInfo( info );
        
      } );
    }
  } , [ authState, authService ] ); 

  let extraModalReference = useCallback( () => {
    if( document.querySelector("#exampleModal") !== null ){
      newModalRef.current = document.querySelector("#exampleModal"); 
    }
    return () => {
      newModalRef.current = null;
    };
  } , [] );

  useEffect( () => {
    checkUser();
    extraModalReference();
  } , [ checkUser, extraModalReference ] );

  let errorCB = () => { console.log("There are errors!"); };

  let completedCB = () => { 
    // console.log("Mutation has completed"); 
    toggleModalClosed();
  };

  const toggleModalClosed = () => {
    $(newModalRef.current).modal( 'hide' );
  };

  const [
    modifyPosts
    , { loading, error, called } 
  ] = useMutation( 
    SUBMIT_POST
    , { 
      onCompleted: completedCB
      , onError: errorCB

      , update( cache , { data: { submitPost } } ){ 

        cache.modify( {
          
          fields: {
            posts( existingPosts = [] ) {
              
              const newPostRef = cache.writeFragment( {
              
                data: submitPost
                
                , fragment: gql`
                  fragment NewPost on Post {
                    id
                    author
                    content
                  }
                `
              } );

              return [ ...existingPosts, newPostRef ];

            }
          }

        } ); 

      } // end update(){}

    } 
  ); 

  const onChangePostContent = ( event ) => { 
    setContent( textAreaRef.current.value ); 
  };

  const handleChange = async ( event ) => {
    setSelectedAuthorString( selectItemRef.current.value ); 
    if( selectItemRef.current.value !== "" ){
      setAuthor( JSON.parse( selectItemRef.current.value ) );
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    
    let postinput = { author: {...author} , content: content }; 

    modifyPosts( {
      variables: { postinput: postinput }
      , optimisticResponse: {
        __typename: "Mutation"
        , submitPost: {
          __typename: "Post"
          , id: undefined
          , author: postinput.author
          , content: postinput.content
        }
      }
    } );

  }; 


  let id;

  if( userInfo !== null ){
    id = userInfo.sub;
  }  
  
  let { loading: singleauthorLoading, error: singleauthorError, data: singleauthorData , networkStatus } = useQuery(
    GET_ONE_AUTHOR
    , {
      variables: { 
        id: id
      }
    } 
  );

  if( networkStatus <= 6 && singleauthorLoading ){
    
    // console.log("networkStatus" , networkStatus );
    // console.log("Loading, baby");

    return <DisclaimerLoading />;
  }

  if( networkStatus === 8 && singleauthorError ){
    
    // console.log("networkStatus" , networkStatus );
    // console.log("singleauthorError", singleauthorError );
    // console.log("Everything is fine... Situation normal... How are you???");

    return <DisclaimerError error={singleauthorError} />;

  }

  if( networkStatus === 7 && singleauthorData !== undefined ){
    
    // console.log("networkStatus" , networkStatus );
    // console.log("singleauthorData" , singleauthorData );
    // console.log("singleauthorData.author" , singleauthorData.author );

    let authors = [];
    authors.push( singleauthorData.author );

    let options = authors.map( ( author ) => {
      return (<SharedOptionItem key={ author.id } author={ author } />);
    } );

    let el = (
      <>
        <div className="container">

          <form className="form" onSubmit={ handleSubmit } >
            
            <div className="form-group">
              <label htmlFor="inputExistingAuthor">Author</label>
              <select id="inputExistingAuthor" onChange={ handleChange } className="form-control rounded-0" value={ selectedAuthorString } ref={ selectItemRef }>
                <option defaultValue="" value="" >Choose an Author</option>
                { options }
              </select>
            </div>

            <div className="form-group">
              <label htmlFor="content">Post Content</label>
              <textarea ref={textAreaRef} value={ content } onChange={ onChangePostContent } className="form-control rounded-0" rows="2" name="content" id="content" placeholder="Bla, bla, bla!!!"></textarea>
            </div>

            <div className="form-group">
              
              <button type="submit" disabled={ content === '' || selectedAuthorString === '' } className="btn btn-primary float-right mr-1">Submit</button>

              <button type="button" className="btn btn-secondary float-right mr-1" onClick={toggleModalClosed}>Cancel</button>
            </div>

          </form>

          { called && (<DisclaimerMutationCalled />) }
          
          { loading && (<DisclaimerLoading />) }

          { error && (<DisclaimerError error={error} />) }

        </div>
      </>
    );

    return el;

  }
  
};

export { AddForm };
