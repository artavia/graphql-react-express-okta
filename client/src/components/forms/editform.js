import React, { useState, useRef, useEffect, useCallback } from "react";
import $ from 'jquery';
import { gql, useMutation } from "@apollo/client";

import { DisclaimerLoading } from "./../elements/disclaimerloading";
import { DisclaimerMutationCalled } from "./../elements/disclaimermutationcalled";
import { DisclaimerError } from "./../elements/disclaimererror";
import { SUBMIT_POST } from "./../../models-graphql-endpoints/mutation-submitpost";

const EditForm = ( props ) => { 

  let { post } = props; 
  
  let { finalizePostHandler } = props;   

  let [ id, setId ] = useState(null);
  let [ authorId, setAuthorId ] = useState(null);
  let [ name, setName ] = useState(null);
  let [ content, setContent ] = useState(""); 
  
  let newModalRef = useRef(null);
  let textAreaRef = useRef(); 
  let hiddenPostIdRef = useRef(); 
  let hiddenAuthorIdRef = useRef(); 
  let hiddenAuthorNameRef = useRef(); 

  let extraModalReference = useCallback( () => {
    if( document.querySelector("#exampleModal") !== null ){
      newModalRef.current = document.querySelector("#exampleModal"); 
    }
    return () => {
      newModalRef.current = null;
    };
  } , [] );

  let statefulmarkers = useCallback( () => {

    setId(post.id);
    setAuthorId(post.author.id);
    setName(post.author.name);
    setContent(post.content); 

    return () => {
      setId(null);
      setAuthorId(null);
      setName(null);
      setContent(null); 
    };

  } , [ post ] );

  useEffect( () => {
    extraModalReference();
    statefulmarkers();
  } , [ extraModalReference , statefulmarkers ] );

  
  let errorCB = () => { console.log("There are errors!"); }; 

  let completedCB = () => { 
    // console.log("Mutation has completed"); 
    toggleModalClosed();
  };

  const toggleModalClosed = () => {
    $(newModalRef.current).modal( 'hide' );
    finalizePostHandler();
  };  

  const [
    modifyPosts
    , { loading, error, called } 
  ] = useMutation( 
    SUBMIT_POST
    , { 
      onCompleted: completedCB
      , onError: errorCB      

      , update( cache , { data: { submitPost } } ){

        cache.modify( {
          
          fields: {
            posts( existingPosts = [] , { readField } ) {
              
              const newPostRef = cache.writeFragment( {

                data: submitPost

                , fragment: gql`
                  fragment NewPost on Post {
                    id
                    author
                    content
                  }
                `
              } );

              if( existingPosts.some( ref => readField( "id", ref ) === newPostRef.id ) ){
                return existingPosts;
              }
              if( !existingPosts.some( ref => readField( "id", ref ) === newPostRef.id ) ){
                return [ ...existingPosts, newPostRef ];
              }

            }
          }

        } );
        
      } // end update(){}

    } 
  ); 

  const onChangePostContent = ( event ) => { 
    setContent( textAreaRef.current.value ); 
  };

  const handleSubmit = async (event) => {

    event.preventDefault();

    let postinput = { 
      id: (hiddenPostIdRef.current !== undefined ) ? hiddenPostIdRef.current.value : null
      , author: { 
        id: (hiddenAuthorIdRef.current !== undefined ) ? hiddenAuthorIdRef.current.value : null 
        , name: (hiddenAuthorNameRef.current !== undefined ) ? hiddenAuthorNameRef.current.value : null
      }
      , content: (textAreaRef.current !== undefined ) ? textAreaRef.current.value : null
    };
    
    modifyPosts( {
      variables: { postinput: postinput }
      , optimisticResponse: {
        __typename: "Mutation"
        , submitPost: {
          __typename: "Post"
          , id: postinput.id
          , author: postinput.author
          , content: postinput.content
        }
      }
    } );

  }; 

  let customform = (
    <>
      <div className="container">
        
        <form className="form" onSubmit={ handleSubmit } >
          
          <div className="form-group">
            <label htmlFor="content">
              <h5 className="h4">Post Number <code>{id}</code></h5>
              <h6 className="h5">by {name} (<code>{authorId}</code>) </h6>
            </label>
            <input type="hidden" ref={hiddenPostIdRef} name={`id-${id}`} id={`id-${id}`} defaultValue={ id } />
            <input type="hidden" ref={hiddenAuthorIdRef} name={`id-${authorId}`} id={`id-${authorId}`} defaultValue={ authorId } />
            <input type="hidden" ref={hiddenAuthorNameRef} name={`id-${name}`} id={`id-${name}`} defaultValue={ name } />
            <textarea ref={textAreaRef} name="content" id="content" value={ content } onChange={ onChangePostContent } className="form-control rounded-0" rows="2"></textarea>
          </div>

          <div className="form-group">
            <button type="submit" disabled={ content === '' } className="btn btn-primary float-right mr-1">Submit</button>
            <button type="button" className="btn btn-secondary float-right mr-1" onClick={toggleModalClosed}>Cancel</button>
          </div>

        </form>

        { called && (<DisclaimerMutationCalled />) }

        { loading && (<DisclaimerLoading />) }

        { error && (<DisclaimerError error={error} />) }
        
      </div>
    </>
  );

  return customform;
  
};

export { EditForm };
