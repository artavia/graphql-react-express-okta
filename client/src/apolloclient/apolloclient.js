import { ApolloClient, InMemoryCache, HttpLink, ApolloLink, from } from "@apollo/client";

const cache = new InMemoryCache( {
  
  typePolicies: {
    
    Query: {
      fields: {
        posts: {           
          merge: false 
        }
        
        , authors: {           
          merge: false 
        }
      }
    }
    , Author: { 
      fields: ["id", "name"]
    }    
    , Post: {
      fields: ["id", "author", "content"]
    }

  }
  
} );

const httpLink = new HttpLink( {
  uri: `http://localhost:4000/graphql` 
} );

const authLink = new ApolloLink( (operation, forward) => {

  operation.setContext( ( { headers = {} } ) => {
    let authHeaders = {};
    if( localStorage.getItem("token") ){
      authHeaders.authorization = `Bearer ${localStorage.getItem("token")}`;
    }
    return ({ 
      headers: {
        ...headers
        , ...authHeaders
      } 
    });
  } ); 
  return forward(operation);
} ); 

const testLink = new ApolloLink( (operation, forward) => { 
  operation.setContext( ( { headers = {} } ) => {
    return ({ 
      headers: {
        ...headers
        , "custom-name": "WidgetX Ecom [web]"
        // , "custom-version": "1.51.50"
        , "custom-version": "5.1.50.ou812"
      }
    });
  } );
  return forward(operation);
} );

const contentTypeLink = new ApolloLink( (operation, forward) => { 
  operation.setContext( ( { headers = {} } ) => {
    return ({ 
      headers: {
        ...headers
        , "Content-Type": "application/graphql; charset=utf-8"
        , "Accept": "application/graphql"        
      }
    });
  } );
  return forward(operation);
} );


const client = new ApolloClient( {

  cache: cache

  , link: from( [ authLink, testLink, contentTypeLink, httpLink ] )  
  
  , defaultOptions: {
    watchQuery: {      
      notifyOnNetworkStatusChange: true 
      , errorPolicy: "all" 
      , fetchPolicy: "cache-and-network"
    }
    , query: {
      errorPolicy: "all"
      , fetchPolicy: "network-only"
    }
    , mutate: { 
      errorPolicy: "none" 
      , fetchPolicy: "no-cache" 

    }
  }

} );

export { client };
