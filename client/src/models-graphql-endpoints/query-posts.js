import { gql } from "@apollo/client";

const GET_POSTS = gql`
  query GetPosts {
    posts {
      id
      author{
        id
        name
      }
      content
    }
  }
`;

export { GET_POSTS };