import { gql } from "@apollo/client";

const GET_ONE_AUTHOR = gql`
  query GetOneAuthor ($id: ID! ) {
    author(id: $id){
      id
      name
    }
  }
`;

export { GET_ONE_AUTHOR };
