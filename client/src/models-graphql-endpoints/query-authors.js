import { gql } from "@apollo/client";

const GET_AUTHORS = gql`
  query GetAuthors {
    authors{
      id
      name
    }
  }
`;

export { GET_AUTHORS };