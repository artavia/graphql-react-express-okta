import { gql } from "@apollo/client";

const GET_ONE_POST = gql`
  query GetOnePost( $id: ID! ) {
    post(id: $id){
      id
      author{
        id
        name
      }
      content
    }
  }
`;

export { GET_ONE_POST };
