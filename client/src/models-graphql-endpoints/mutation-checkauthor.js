import { gql } from "@apollo/client";

const CHECK_AUTHOR = gql`
  mutation CheckAuthor($authorinput: AuthorInput!){
    checkAuthor( input: $authorinput ){
      id
      name
    }
  }
`; 

export { CHECK_AUTHOR };
