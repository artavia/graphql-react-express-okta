import { gql } from "@apollo/client";

const SUBMIT_POST = gql`
  mutation SubmitPost( $postinput: PostInput! ){
    submitPost( input: $postinput ){
      id
      author{
        id
        name
      }
      content
    }
  }
`;

export { SUBMIT_POST };
